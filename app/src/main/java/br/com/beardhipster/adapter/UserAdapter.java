package br.com.beardhipster.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.app.beardhipster.R;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.activity.EditProfileActivity;
import br.com.beardhipster.activity.ViewProfileActivity;
import br.com.beardhipster.model.User;
import br.com.beardhipster.task.MatchLikedTask;
import br.com.beardhipster.util.SessionManager;
import br.com.beardhipster.util.Util;

/**
 * Created by cassiopaixao on 01/05/2017.
 */

public class UserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<User> users;
    private SessionManager manager;

    private Integer position;

    public UserAdapter() {
        this.users = new ArrayList<>();
    }

    public UserAdapter(Context context, List<User> users) {
        this.users = users;
        this.context = context;
        this.manager = new SessionManager(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context)
                .inflate(R.layout.recycle_items, parent, false);
        UserViewHolder holder = new UserViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        this.position = position;
        final User user = users.get(position);

        final UserViewHolder holder = (UserViewHolder) viewHolder;

        String age = "";
        if (user.getIdade() != null && !user.getIdade().equalsIgnoreCase("null")){
            holder.titleTextView.setText(user.getName() + ", " + user.getIdade());
        }else{
            holder.titleTextView.setText(user.getName());
        }

        //holder.coverImageView.setImageResource(user.getImageResourceId());
        if(user.isMatched()) {
            holder.likeImageView.setImageResource(R.drawable.ic_liked);
            holder.likeImageView.setTag(R.drawable.ic_liked);
        }else {
            holder.likeImageView.setImageResource(R.drawable.ic_like);
            holder.likeImageView.setTag(R.drawable.ic_like);
        }
        holder.likeImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = (int) holder.likeImageView.getTag();
                if (id == R.drawable.ic_like) {

                    holder.likeImageView.setTag(R.drawable.ic_liked);
                    holder.likeImageView.setImageResource(R.drawable.ic_liked);

                    //String idUser = manager.getUid();
                    //String idUserLiked = String.valueOf(user.getId());
                    //Toast.makeText(context, "Id" + user.getId(), Toast.LENGTH_LONG).show();

                    user_match(Util.URL_MATCH,user);
                } else {

                    holder.likeImageView.setTag(R.drawable.ic_like);
                    holder.likeImageView.setImageResource(R.drawable.ic_like);
                    //Toast.makeText(context, "Id" + user.getId(), Toast.LENGTH_LONG).show();
                    user_match(Util.URL_UNMATCH,user);
                }

            }
        });


        if (user.getUrlImagem() != null) {
            Picasso.with(context)
                    .load(user.getUrlImagem())
                    .into(holder.coverImageView);
        } else {
            holder.coverImageView.setImageResource(R.drawable.beard_1);
        }
        holder.coverImageView.setTag(user.getName());
        holder.coverImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ViewProfileActivity.class);
                intent.putExtra("user", user);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextView;
        public ImageView coverImageView;
        public ImageView likeImageView;

        public UserViewHolder(View v) {
            super(v);
            titleTextView = (TextView) v.findViewById(R.id.titleTextView);
            coverImageView = (ImageView) v.findViewById(R.id.coverImageView);
            likeImageView = (ImageView) v.findViewById(R.id.likeImageView);

        }
    }

    public void user_match(String URL, User userliked){
        String idUser = manager.getUid();
        RequestPackage p = new RequestPackage();
        p.setUri(URL);
        p.setMethod("POST");
        p.setParam("id1", idUser);
        p.setParam("id2", String.valueOf(userliked.getId()));
        MatchLikedTask loginTask = new MatchLikedTask(context);
        loginTask.execute(p);
    }
}
