package br.com.beardhipster.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import br.com.app.beardhipster.R;
import br.com.beardhipster.activity.MainActivity;
import br.com.beardhipster.config.AppController;

/**
 * Created by cassiopaixao on 30/04/2017.
 */

public class CheckInternetBroadcastReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
       int type [] = {ConnectivityManager.TYPE_MOBILE, ConnectivityManager.TYPE_WIFI};
        if(isNetWorkAvaliable(context,type) == true){
            return;
        }else {
            Toast.makeText(context,context.getString(R.string.no_internet_message), Toast.LENGTH_LONG ).show();
        }
    }

    private boolean isNetWorkAvaliable(Context context, int[] typeNetWorks) {

        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            //for (int typeNet : typeNetWorks) {
                //NetworkInfo networkInfo = cm.getNetworkInfo(typeNet);
                NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            //}

        }catch (Exception e){
            return false;
        }
        return false;
    }
}