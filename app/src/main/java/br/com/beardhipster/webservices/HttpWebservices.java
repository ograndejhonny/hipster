package br.com.beardhipster.webservices;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.beardhipster.converter.Convert;
import br.com.beardhipster.model.User;
import br.com.beardhipster.util.UserCallback;
import br.com.beardhipster.util.Util;

/**
 * Created by cassiopaixao on 29/04/2017.
 */

public abstract class HttpWebservices {


    public static void sendParameter(String method, User user, String url, boolean doOuput, HashMap<String, String> values, UserCallback userCallback) {

        String s = null;
        JSONObject jsonObject = null;

        try {
            HttpURLConnection connection = openConnection(url, method, doOuput);
            if (doOuput) {
                OutputStream os = connection.getOutputStream();
                OutputStreamWriter osWriter = new OutputStreamWriter(os, "UTF-8");
                BufferedWriter writer = new BufferedWriter(osWriter);
                writer.write(getPostData(values));
                writer.flush();
                writer.close();
                os.close();
            }

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = connection.getInputStream();
                s = Convert.streamToString(inputStream);
                userCallback.result(s);
                inputStream.close();
            }
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static HttpURLConnection openConnection(String url, String method, boolean doOuput) throws Exception {

        URL urlCon = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) urlCon.openConnection();
        connection.setReadTimeout(1500);
        connection.setConnectTimeout(1500);
        connection.setRequestMethod(method);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.connect();
        return connection;
    }


    public static List<User> getUsers() throws Exception {
        HttpURLConnection con = openConnection(Util.URL_USERS, "GET", false);

        List<User> list = new ArrayList<User>();
        //if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
            String jsonString = Convert.streamToString(con.getInputStream());
            list = Convert.jsonToObject(jsonString);
        //}
        return list;
    }

    public static String getPostData(HashMap<String, String> values) {
        StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : values.entrySet()) {
            if (first)
                first = false;
            else
                builder.append("&");
            try {
                builder.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                builder.append("=");
                builder.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            catch (UnsupportedEncodingException e) {}
        }
        return builder.toString();
    }

}
