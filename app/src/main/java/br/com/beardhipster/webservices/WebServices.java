package br.com.beardhipster.webservices;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.beardhipster.config.AppController;
import br.com.beardhipster.model.User;
import br.com.beardhipster.util.AuthCallback;
import br.com.beardhipster.util.JSonCallback;
import br.com.beardhipster.util.Util;
import okhttp3.OkHttpClient;

/**
 * Created by cassiopaixao on 22/04/2017.
 */

public abstract class WebServices {

    public static void registerUser(final User usuario, final AuthCallback authCallback) {
        String tag_string_req = "req_register";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Util.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        authCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        authCallback.onFailure(error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", usuario.getName());
                params.put("email", usuario.getEmail());
                params.put("password", usuario.getPassword());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, tag_string_req);
    }

    public static void loginUser(final User usuario, final AuthCallback authCallback) {
        String tag_string_req = "req_register";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Util.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        authCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        authCallback.onFailure(error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", usuario.getEmail());
                params.put("password", usuario.getPassword());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, tag_string_req);
    }

    public static void list(final JSonCallback jSonCallback) {


        JsonArrayRequest userReq = new JsonArrayRequest(Util.URL_USERS,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        jSonCallback.onJson(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                jSonCallback.onVolleyError(error);
            }
        });
        AppController.getInstance().addToRequestQueue(userReq);
    }

    public static void resetPassword(final User usuario, final AuthCallback authCallback) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Util.URL_RESET_PASSWORD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        authCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        authCallback.onFailure(error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", usuario.getEmail());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public static void updateUser(final User user, final AuthCallback authCallback){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Util.URL_UPDATE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        authCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        authCallback.onFailure(error);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("uid", user.getUid());
                params.put("password", user.getPassword());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);

    }
}
