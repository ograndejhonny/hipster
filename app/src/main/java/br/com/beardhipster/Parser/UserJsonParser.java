package br.com.beardhipster.Parser;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import br.com.beardhipster.model.CallbackJson;
import br.com.beardhipster.model.User;
import br.com.beardhipster.type.SexEnum;
import br.com.beardhipster.util.DateUtils;
import br.com.beardhipster.util.StringUtils;

/**
 * Created by cassiopaixao on 30/04/2017.
 */

public class UserJsonParser {


    public static List<User> getUsers(String result) {

        List<User> listUsers = new ArrayList<User>();
        if (result != null) {
            try {
                JSONObject jsonObj = new JSONObject(result);
                JSONArray users = jsonObj.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    User user = new User();
                    JSONObject c = users.getJSONObject(i);
                    user.setId(c.getInt("user_id"));
                    user.setName(c.getString("name"));
                    user.setEmail(c.getString("email"));
                    user.setUrlImagem(c.getString("url_image"));
                    user.setIdade(c.getString("user_age"));
                    try {
                        user.setMatched(c.getString("has_matched").equals("1"));
                    } catch (Exception e){
                        e.getMessage();
                    }

                    listUsers.add(user);
                }
            } catch (final JSONException e) {
                e.printStackTrace();
            }
        }
        return listUsers;

    }

    public static User getUser(String json) {
        User user = new User();
        CallbackJson callbackJson = new CallbackJson();
        if (json != null) {
            try {

                JSONObject jObj = new JSONObject(json);
                callbackJson.setError(jObj.getBoolean("error"));
                callbackJson.setMessage(jObj.getString("message"));
                if( !callbackJson.isError() ){
                    JSONObject jsonObject = jObj.getJSONObject("user");
                    user.setId(jsonObject.getLong("user_id"));
                    user.setName(jsonObject.getString("name"));
                    user.setEmail(jsonObject.getString("email"));
                    try {
                        user.setMatched(jsonObject.getString("has_matched").equals("1"));
                    } catch (Exception e){
                        //e.getMessage();
                    }
                }
                user.setCallbackJson(callbackJson);
            } catch (final JSONException e) {
                e.printStackTrace();
                Log.e("br.com.app.beardhipster", e.getMessage());
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return user;
    }


    public static User getFullUser(String json) {
        User user = new User();
        CallbackJson callbackJson = new CallbackJson();
        if (json != null) {
            try {

                json  = json.replace("\r", "");
                json  = json.replace("\n", "");
                JSONObject jObj = new JSONObject(json.replace("string(0)", "").replace("\"\"",""));
                callbackJson.setError(jObj.getBoolean("error"));
                callbackJson.setMessage(jObj.getString("message"));
                Log.e("br.com.app.beardhipster", jObj.getString("message"));
                if( !callbackJson.isError() ){
                    JSONObject jsonObject = jObj.getJSONObject("user");
                    user.setId(jsonObject.getLong("user_id"));
                    user.setName(jsonObject.getString("name"));
                    user.setEmail(jsonObject.getString("email"));
                    user.setUrlImagem(jsonObject.getString("url_image"));
                    user.setBirthDate(DateUtils.parseStringToDate(jsonObject.getString("birth_date")));

                    String minAge = jsonObject.getString("min_age");
                    if (StringUtils.isValid(minAge) && !minAge.equalsIgnoreCase("null"))
                        user.setMinAge(Integer.parseInt(minAge));

                    String maxAge = jsonObject.getString("max_age");
                    if (StringUtils.isValid(maxAge) && !maxAge.equalsIgnoreCase("null"))
                        user.setMaxAge(Integer.parseInt(maxAge));

                    try {
                        user.setMatched(jsonObject.getString("has_matched").equals("1"));
                    } catch (Exception e){
                        //e.getMessage();
                    }
                }
                user.setCallbackJson(callbackJson);
            } catch (final JSONException e) {
                e.printStackTrace();
                Log.e("br.com.app.beardhipster", e.getMessage());
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return user;
    }
}
