package br.com.beardhipster.HttpManager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cassiopaixao on 30/04/2017.
 */

public class RequestPackage {

    private String uri;
    private String method = "GET";
    private Map<String, String> param = new HashMap<String, String>();

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, String> getParam() {
        return param;
    }

    public void setParam(Map<String, String> param) {
        this.param = param;
    }

    public void setParam(String key, String valor) {
        param.put(key, valor);
    }

    public String getEncondedParams() {
        StringBuilder builder = new StringBuilder();
        String value = null;
        for (String key : param.keySet()) {
            try {
                value = URLEncoder.encode(param.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(key + "=" + value);
        }
        return builder.toString();
    }

}
