package br.com.beardhipster.HttpManager;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.beardhipster.util.AuthCallback;
import br.com.beardhipster.util.Util;

/**
 * Created by cassiopaixao on 30/04/2017.
 */

public class HttpManager {

    private static Context context;
    private static String content;

    public HttpManager(Context context) {
        this.context = context;
    }


    public static String getData(RequestPackage p) {

        BufferedReader br = null;
        String uri = p.getUri();

        if (p.getMethod().equals("GET") && !p.getEncondedParams().isEmpty()) {
            uri += "?" + p.getEncondedParams();
        }

        try {
            URL url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(p.getMethod());

            if (p.getMethod().equals("POST")) {
                con.setDoOutput(true);
                OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());
                out.write(p.getEncondedParams());
                out.flush();
            }

            StringBuilder sb = new StringBuilder();
            br = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            return sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getUsers(Context context) {

        try{
            StringRequest stringRequest = new StringRequest(Util.URL_USERS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    content = response;
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    content = error.getMessage();
                }
            });
            RequestQueue queue = Volley.newRequestQueue(context);
            queue.add(stringRequest);
        }catch (Exception e){
            e.printStackTrace();
        }
        return content;
    }


}
