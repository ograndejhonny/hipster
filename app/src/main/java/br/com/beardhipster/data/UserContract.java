package br.com.beardhipster.data;

public final class UserContract {

	public static final String TABLE_NAME = "user";
	
	public static final class Columns {
		public static final String _ID = "_id";
		public static final String NOME = "nome";
		public static final String EMAIL = "email";
	}
}
