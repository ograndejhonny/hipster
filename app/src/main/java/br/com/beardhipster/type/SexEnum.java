package br.com.beardhipster.type;

/**
 * Created by jsramos on 05/05/2017.
 */

public enum SexEnum {
    NAOESPECIFICADO("Não Especificado", 3),
    MASCULINO("Masculino", 1),
    FEMININO("Feminino", 2);

    private String stringValue;
    private int intValue;

    private SexEnum(String toString, int value) {
        stringValue = toString;
        intValue = value;
    }

    @Override
    public String toString() {
        return stringValue;
    }

}
