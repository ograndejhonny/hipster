package br.com.beardhipster.task;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import br.com.beardhipster.HttpManager.HttpManager;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.Parser.UserJsonParser;
import br.com.beardhipster.activity.MainActivity;
import br.com.beardhipster.model.User;
import br.com.beardhipster.util.SessionManager;

/**
 * Created by cassiopaixao on 29/04/2017.
 */

public class SignupTask extends AsyncTask<RequestPackage, User, User> {

    private Context context;
    ProgressBar progressBar;
    private String content;
    protected SessionManager manager;

    public SignupTask(Context context, ProgressBar progressBar) {
        this.context = context;
        this.progressBar = progressBar;
        this.progressBar.setVisibility(View.INVISIBLE);
        this.manager = new SessionManager(context);
    }

    @Override
    protected User doInBackground(RequestPackage... params) {
        content = HttpManager.getData(params[0]);
        User user = UserJsonParser.getUser(content);
        return user;
    }

    @Override
    protected void onPreExecute() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(User user) {
        if(!user.getCallbackJson().isError()){
            manager.setLogin(true);
            manager.setUid(String.valueOf(user.getId()));
            context.startActivity(new Intent(context, MainActivity.class));
            ((Activity)context).finish();
        }else{
            progressBar.setVisibility(View.INVISIBLE);
            updateDisplay(user.getCallbackJson().getMessage());
        }
    }

    private void updateDisplay(String s) {
        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }

}
