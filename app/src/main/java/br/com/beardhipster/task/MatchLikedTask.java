package br.com.beardhipster.task;

import android.content.Context;
import android.os.AsyncTask;

import br.com.beardhipster.HttpManager.HttpManager;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.Parser.UserJsonParser;
import br.com.beardhipster.model.User;

/**
 * Created by cassiopaixao on 03/05/2017.
 */

public class MatchLikedTask extends AsyncTask<RequestPackage, Void, User> {

    private String content;
    private Context context;

    public MatchLikedTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected User doInBackground(RequestPackage... params) {
        content = HttpManager.getData(params[0]);
        User user = UserJsonParser.getUser(content);
        return user;
    }

    @Override
    protected void onPostExecute(User aVoid) {
        super.onPostExecute(aVoid);
    }
}
