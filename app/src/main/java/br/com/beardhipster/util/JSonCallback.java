package br.com.beardhipster.util;

import com.android.volley.VolleyError;

import org.json.JSONArray;

/**
 * Created by cassiopaixao on 23/04/2017.
 */

public interface JSonCallback  {

    void onJson(JSONArray  jsonArray);
    void onVolleyError(VolleyError volleyError);

}
