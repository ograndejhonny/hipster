package br.com.beardhipster.util;

import java.util.List;

import br.com.beardhipster.model.User;

/**
 * Created by cassiopaixao on 29/04/2017.
 */

public interface UserCallback {

    void result(String string);
}
