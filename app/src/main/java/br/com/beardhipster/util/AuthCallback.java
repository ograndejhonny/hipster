package br.com.beardhipster.util;

import com.android.volley.VolleyError;

import org.json.JSONArray;

/**
 * Created by Cássio Paixao
 */
public interface AuthCallback {
    void onSuccess(String response);
    void onFailure(VolleyError message);
}
