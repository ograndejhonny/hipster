package br.com.beardhipster.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by cassiopaixao on 14/04/2017.
 */

public abstract class
Util {

    public static final String PREF = "br.com.beardhipster.PREF";
    public static final String CHILD_USERS = "Users";
    public static final String PROVIDER = "br.com.beardhipster.model.User.PROVIDER";

    public static String URL_LOGIN = "http://beardapp.esy.es/beardhipster/v1/user/login";
    //public static String URL_REGISTER = "http://10.0.2.2:8080/beardhipster/v1/user/signup";
    public static String URL_USERS = "http://beardapp.esy.es/beardhipster/v1/user/all";
    public static String URL_RESET_PASSWORD = "http://10.0.2.2:8080/beardhipster/resetPassword.php";
    public static String URL_UPDATE = "http://10.0.2.2:8080/beardhipster/beardhipster/v1/user/signup";
    public static String URL_REGISTER = "http://beardapp.esy.es/beardhipster/v1/user/signup";
    public static String URL_MATCH ="http://beardapp.esy.es/beardhipster/v1/user/match";
    public static String URL_UNMATCH ="http://beardapp.esy.es/beardhipster/v1/user/unmatch";
    public static String URL_GET_USER = "http://beardapp.esy.es/beardhipster/v1/user/id";


    public static final String UPLOAD_URL = "http://beardapp.esy.es/beardhipster/v1/user/image";
    public static final String UPDATE_URL = "http://beardapp.esy.es/beardhipster/v1/user/edit";


    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
