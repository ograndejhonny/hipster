package br.com.beardhipster.converter;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.beardhipster.model.User;

/**
 * Created by cassiopaixao on 22/04/2017.
 */

public abstract class ConvertJson {

    public static User converterJsonObject(String json) {
        User user = new User();
        try {
            JSONObject jObj = new JSONObject(json);
            boolean error = jObj.getBoolean("error");
            if (!error) {
                user.setLogado(true);
                JSONObject jsonObject = jObj.getJSONObject("user");
                //user.setUid(jsonObject.getString("user_id"));
                user.setName(jsonObject.getString("name"));
                user.setEmail(jsonObject.getString("email"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return user;
    }

    private static User login(String json) {
        String tag_string_req = "req_login";
        User user = new User();
        try {
            JSONObject jObj = new JSONObject(json);
            boolean error = jObj.getBoolean("error");
            if (!error) {
                user.setLogado(true);
                JSONObject jsonObject = jObj.getJSONObject("user");
                //user.setUid(jsonObject.getString("user_id"));
                user.setName(jsonObject.getString("name"));
                user.setEmail(jsonObject.getString("email"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return user;
    }

    public static List<User> users(JSONArray jsonArray) {
        List<User> users = new ArrayList<User>();
        try {

            /*
            for (int i = 0; i < jsonArray.length(); i++) {
                User user = new User();
                JSONObject jObj = jsonArray.getJSONObject(i);
                user.setUid(jsonObject.getString("user_id"));
                user.setName(jsonObject.getString("name"));
                user.setEmail(jsonObject.getString("email"));
                user.setUrlImagem(jsonObject.getString("url_imagem"));
                users.add(user);
            }
            */
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("JsonArray", e.getMessage());
        }
        return users;
    }

    public static User resetPasswordJsonObject(String json) {
        User user = new User();
        try {
            JSONObject jObj = new JSONObject(json);
            boolean exist = jObj.getBoolean("exist");
            if (exist) {
                String uid = jObj.getString("uid");
                //user.setUid(uid);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return user;
    }
}