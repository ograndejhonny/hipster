package br.com.beardhipster.converter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.beardhipster.model.User;

/**
 * Created by cassiopaixao on 29/04/2017.
 */

public abstract class Convert {

    public static byte[] UserToJsonBytes(User user) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", user.getName());
            jsonObject.put("email", user.getEmail());
            jsonObject.put("password", user.getPassword());
            String json = jsonObject.toString();
            return json.getBytes();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String streamToString(InputStream in) throws IOException {
        byte[] bytes = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int lidos;
        while ((lidos = in.read(bytes)) > 0) {
            baos.write(bytes, 0, lidos);
        }
        return new String(baos.toByteArray());
    }

    public static List<User> jsonToObject(String json) {

        List<User> listUsers = new ArrayList<User>();
        if (json != null) {
            try {
                JSONObject jsonObj = new JSONObject(json);
                JSONArray users = jsonObj.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    User user = new User();
                    JSONObject c = users.getJSONObject(i);
                    user.setId(c.getInt("user_id"));
                    user.setName(c.getString("name"));
                    user.setEmail(c.getString("email"));
                    listUsers.add(user);
                }
            } catch (final JSONException e) {
                e.printStackTrace();
            }
        }
        return listUsers;
    }

    public static User jsonToUser(String json) {
        User user = null;
        if (json != null) {
            try {
                JSONObject jObj = new JSONObject(json);
                JSONObject jsonObject = jObj.getJSONObject("user");
               // user.setUid(jsonObject.getString("user_id"));
                user.setName(jsonObject.getString("name"));
                user.setEmail(jsonObject.getString("email"));
            } catch (final JSONException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
