package br.com.beardhipster.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import br.com.app.beardhipster.R;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.data.UserDAO;
import br.com.beardhipster.model.User;
import br.com.beardhipster.task.SignupTask;
import br.com.beardhipster.util.SessionManager;
import br.com.beardhipster.util.Util;
import br.com.beardhipster.validator.Validator;

public class SignupActivity extends AppCompatActivity {
    private EditText etName, etEmail, etPassword;
    private User user;
    UserDAO userDAO;
    private SessionManager session;

    protected ArrayAdapter<String> adapter;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initViews();
        session = new SessionManager(getApplicationContext());
        userDAO = UserDAO.getInstance(this);
    }

    public void initViews() {
        etName = (EditText) findViewById(R.id.et_nome);
        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_senha);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void initUser() {
        user = new User();
        user.setName(etName.getText().toString().trim());
        user.setEmail(etEmail.getText().toString().trim());
        user.setPassword(etPassword.getText().toString().trim());
    }

    public void goToRegister(View view) {

        if (!Util.isOnline(this)) {
            Toast.makeText(this, "NetWorks isn't Avaliable", Toast.LENGTH_LONG).show();
        } else {
            requestData();
        }
    }

    private void requestData() {
        initUser();
        if (!formIsValid()) {
            return;
        }
        RequestPackage p = new RequestPackage();
        p.setMethod("POST");
        p.setUri(Util.URL_REGISTER);
        p.setParam("name", user.getName());
        p.setParam("email", user.getEmail());
        p.setParam("password", user.getPassword());

        SignupTask task = new SignupTask(this, progressBar);
        task.execute(p);

    }

    private void finishLogin() {
        Intent loginIntent = new Intent(this, MainActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    private boolean formIsValid() {
        EditText forms[] = {etName, etEmail, etPassword};
        return Validator.formIsValid(this, forms);
    }

    public void goToResetPassword(View view) {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }

    public void goToLogin(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}