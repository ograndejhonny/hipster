package br.com.beardhipster.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.florescu.android.rangeseekbar.RangeSeekBar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import br.com.app.beardhipster.R;
import br.com.beardhipster.HttpManager.HttpManager;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.Parser.UserJsonParser;
import br.com.beardhipster.model.User;
import br.com.beardhipster.task.LoginTask;
import br.com.beardhipster.task.MatchLikedTask;
import br.com.beardhipster.task.UserLoggedTask;
import br.com.beardhipster.type.SexEnum;
import br.com.beardhipster.util.SessionManager;
import br.com.beardhipster.util.StringUtils;
import br.com.beardhipster.util.Util;
import br.com.beardhipster.validator.Validator;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    //Declaring views
    private Button buttonChoose;
    private Button buttonUpload;
    private ImageView imageView;
    private SessionManager manager;

    private Button btnSalvar;


    private Button buttonRemove;

    private ProgressBar progressbar;

    private Spinner sexType;

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private RangeSeekBar idades;
    private int year, month, day;

    private boolean removeImage = false;

    private Integer minAge, maxAge;


    private EditText name;
    private EditText email;

    public User user;

    private int PICK_IMAGE_REQUEST = 1;

    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    //Bitmap to get image from gallery
    private Bitmap bitmap;

    //Uri to store the image uri
    private Uri filePath;

    //private User user;
    private String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        //Requesting storage permission
        requestStoragePermission();

        //Initializing views
        buttonChoose = (Button) findViewById(R.id.buttonChoose);
        buttonUpload = (Button) findViewById(R.id.buttonUpload);
        buttonRemove = (Button) findViewById(R.id.buttonRemove);
        imageView = (ImageView) findViewById(R.id.imageView);

        buttonRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonRemove.setEnabled(false);
                buttonUpload.setEnabled(false);
                removeImage = true;
                imageView.setImageResource(android.R.color.transparent);
            }
        });

        sexType = (Spinner) findViewById(R.id.sexSpinner);
        sexType.setAdapter(new ArrayAdapter<SexEnum>(this, android.R.layout.simple_list_item_1, SexEnum.values()));

        user_id = (String) getIntent().getSerializableExtra("user_id");

        progressbar = (ProgressBar) findViewById(R.id.progressbar_save);
        progressbar.setVisibility(View.INVISIBLE);

        name = (EditText) findViewById(R.id.txtNome);
        email = (EditText) findViewById(R.id.txtEmail);

        btnSalvar = (Button) findViewById(R.id.btn_save);

        manager = new SessionManager(this);

        try {
            RequestPackage p = new RequestPackage();
            p.setMethod("POST");
            p.setUri(Util.URL_GET_USER);
            p.setParam("user_id", manager.getUid());

            UserLoggedTask task = new UserLoggedTask(this, progressbar);
            user = task.execute(p).get();


            if (user != null){
                name.setText(user.getName());
                email.setText(user.getEmail());
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        dateView = (TextView) findViewById(R.id.txtDate);
        calendar = Calendar.getInstance();

        //showDate(year, month + 1, day);

        idades = (RangeSeekBar) findViewById(R.id.ages);

        if (user.getMinAge() != null){
            idades.setSelectedMinValue(user.getMinAge());
        }

        if (user.getMaxAge() != null){
            idades.setSelectedMaxValue(user.getMaxAge());
        }

        if (user.getBirthDate() != null){
            calendar.setTime(user.getBirthDate());
            year = calendar.get(Calendar.YEAR);

            month = calendar.get(Calendar.MONTH) + 1 ;
            day = calendar.get(Calendar.DAY_OF_MONTH);
            showDate(year, month, day);
        }

        idades.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {

            @Override
            public void onRangeSeekBarValuesChanged(
                    RangeSeekBar<?> bar, Integer minValue,
                    Integer maxValue) {
                minAge = minValue;
                maxAge = maxValue;
                //Log.d("br.com.app.beardhipster", "MinValue: " + minAge);
                //Log.d("br.com.app.beardhipster", "MaxValue: " + maxAge);
            }
        });

        btnSalvar = (Button) findViewById(R.id.btn_save);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtils.isValid(name))
                    saveUser();
            }
        });

        if (user.getUrlImagem() != null) {
            Picasso.with(this)
                    .load(user.getUrlImagem())
                    .into(imageView);
            buttonRemove.setEnabled(true);
            buttonUpload.setEnabled(false);
        } else {
            imageView.setImageResource(R.drawable.beard_1);
            buttonRemove.setEnabled(false);
            buttonUpload.setEnabled(false);
        }


    }

    public void buttonChoose(View view) {
        showFileChooser();
    }

    public void buttonUpload(View view) {
        uploadMultipart();
    }

    /*
    * This is the method responsible for image upload
    * We need the full image path and the name for the image in this method
    * */
    public void uploadMultipart() {

        //getting the actual path of the image
        String path = getPath(filePath);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            UploadNotificationConfig notification = new UploadNotificationConfig();
            notification.setTitle(this.getString(R.string.send_photo_title));
            notification.setInProgressMessage(this.getString(R.string.send_progress_message));
            notification.setCompletedMessage(this.getString(R.string.send_completed_message));
            notification.setErrorMessage(this.getString(R.string.send_error_message));

            //Creating a multi part request
            new MultipartUploadRequest(this, uploadId, Util.UPLOAD_URL)
                    .addFileToUpload(path, "image") //Adding file
                    .addParameter("id", String.valueOf(user_id)) //Adding text parameter to the request
                    .setNotificationConfig(notification)
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload
            buttonUpload.setEnabled(false);
        } catch (Exception exc) {
            Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //method to show file chooser
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);

                buttonRemove.setEnabled(true);
                buttonUpload.setEnabled(true);
                removeImage = false;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            buttonRemove.setEnabled(false);
            buttonUpload.setEnabled(false);
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onClick(View v) {
        if (v == buttonChoose) {
            showFileChooser();
        }
        if (v == buttonUpload) {
            uploadMultipart();
        }
    }


    public void setDate(View view) {

        // Inflate your custom layout containing 2 DatePickers
        LayoutInflater inflater = (LayoutInflater) getLayoutInflater();
        View customView = inflater.inflate(R.layout.custom_date_picker, null);

        // Define your date pickers
        final DatePicker dpStartDate = (DatePicker) customView.findViewById(R.id.datePicker1);
        dpStartDate.updateDate(year,month,day);

        // Build the dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(customView); // Set the view of the dialog to your custom layout
        builder.setTitle("Selecione a data");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                year = dpStartDate.getYear();
                month = dpStartDate.getMonth();
                day = dpStartDate.getDayOfMonth();
                showDate(year,month + 1,day);
                dialog.dismiss();
            }});
        //builder.setCancelable(true);
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });

        // Create and show the dialog
        builder.create().show();
        //showDate(year,month,day);
    }

    private void showDate(int year, int month, int day) {

        dateView.setText(new StringBuilder().append(day < 10? "0" + day: day).append("/")
                .append(month < 10? "0" + month: month).append("/").append(year));
    }

    public void saveUser(){
        String idUser = manager.getUid();
        RequestPackage p = new RequestPackage();
        p.setUri(Util.UPDATE_URL);
        p.setMethod("POST");
        p.setParam("id", idUser);
        p.setParam("birth_date", year + "-" + month  + "-" + day);
        p.setParam("min_age", String.valueOf(minAge));
        p.setParam("max_age", String.valueOf(maxAge));
        p.setParam("has_beard", "false");
        p.setParam("remove_image",  "false");
        p.setParam("sextype", "0");
        UserLoggedTask loginTask = new UserLoggedTask(this, progressbar);
        loginTask.execute(p);
    }


}

