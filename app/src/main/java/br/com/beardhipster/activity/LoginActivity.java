package br.com.beardhipster.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;

import br.com.app.beardhipster.R;
import br.com.beardhipster.HttpManager.RequestPackage;
import br.com.beardhipster.converter.ConvertJson;
import br.com.beardhipster.data.UserDAO;
import br.com.beardhipster.model.User;
import br.com.beardhipster.task.LoginTask;
import br.com.beardhipster.task.SignupTask;
import br.com.beardhipster.util.AuthCallback;
import br.com.beardhipster.util.SessionManager;
import br.com.beardhipster.util.Util;
import br.com.beardhipster.validator.Validator;
import br.com.beardhipster.webservices.WebServices;

public class LoginActivity extends AppCompatActivity {

    private EditText etEmail, etPassword;
    private User user;
    protected UserDAO userDAO;
    protected SessionManager session;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        session = new SessionManager(getApplicationContext());
        if(session.isLoggedIn()){
            callMainActivity();
        }
    }

    public void goToSignup(View view) {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }

    public void btnLogin(View view) {
        initUser();
        if (!formIsValid()) {
            return;
        }
        requestData();
    }

    private void requestData() {
        RequestPackage p = new RequestPackage();
        p.setMethod("POST");
        p.setUri(Util.URL_LOGIN);
        p.setParam("email", user.getEmail());
        p.setParam("password", user.getPassword());

        LoginTask task = new LoginTask(this, progressBar);
        task.execute(p);
    }

    public void initViews() {
        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_senha);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        this.progressBar.setVisibility(View.INVISIBLE);
    }

    public void initUser() {
        user = new User();
        user.setEmail(etEmail.getText().toString().trim());
        user.setPassword(etPassword.getText().toString().trim());
    }

    private void finishLogin() {
        Intent loginIntent = new Intent(this, MainActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }


    private boolean formIsValid() {
        EditText forms[] = {etEmail, etPassword};
        return Validator.formIsValid(this, forms);
    }

    private void callMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void callLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void goToResetPassword(View view) {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }

}

