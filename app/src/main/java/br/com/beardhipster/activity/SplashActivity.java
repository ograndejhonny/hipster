package br.com.beardhipster.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import br.com.app.beardhipster.R;
import br.com.beardhipster.util.SessionManager;

public class SplashActivity extends AppCompatActivity {
    protected Context context;
    protected SharedPreferences mPreferences;
    protected SessionManager manager;
    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        manager = new SessionManager(this);
        if(manager.isLoggedIn() && manager != null){
            callMainActivity();
        }
        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                callLoginActivity();
            }
        }, 3000);

    }


    public void callSpalshActivity() {
        if (mPreferences.contains("1")) {
            callLoginActivity();
        } else {
            saveSharedPrefence();
            Handler handle = new Handler();
            handle.postDelayed(new Runnable() {
                @Override
                public void run() {
                    callLoginActivity();
                }
            }, 3000);
        }
    }

    protected void callMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    protected void callLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();

    }

    protected void saveSharedPrefence() {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString("1", "sim").commit();
    }
}
