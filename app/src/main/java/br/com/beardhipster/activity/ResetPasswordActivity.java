package br.com.beardhipster.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.VolleyError;

import br.com.app.beardhipster.R;
import br.com.beardhipster.converter.ConvertJson;
import br.com.beardhipster.model.User;
import br.com.beardhipster.util.AuthCallback;
import br.com.beardhipster.util.SessionManager;
import br.com.beardhipster.validator.Validator;
import br.com.beardhipster.webservices.WebServices;

public class ResetPasswordActivity extends AppCompatActivity {

    protected EditText etEmail;
    protected User user;
    protected SessionManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initViews();
        manager = new SessionManager(this);
    }

    public void initViews() {
        etEmail = (EditText) findViewById(R.id.et_email);
    }

    public void initUser() {
        user = new User();
        user.setEmail(etEmail.getText().toString().trim());
    }

    public void btnResetarSenha(View view) {

        initUser();
        if (!formIsValid()) {
            return;
        }

        WebServices.resetPassword(user, new AuthCallback() {
            @Override
            public void onSuccess(String response) {
                User user = ConvertJson.resetPasswordJsonObject(response);
                if (user != null) {
//                    /manager.setUid(user.getUid());
                    goToNewPassword();
                } else {
                    Log.d("Email", "usuaario não localizado no sistema");
                }
            }

            @Override
            public void onFailure(VolleyError message) {

            }
        });
    }

    private boolean formIsValid() {
        EditText forms[] = {etEmail};
        return Validator.formIsValid(this, forms);
    }


    protected void goToNewPassword(){
        Intent intent = new Intent(this,NewPasswordActivity.class);
        startActivity(intent);
    }

}
