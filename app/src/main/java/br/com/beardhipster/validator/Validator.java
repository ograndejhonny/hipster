package br.com.beardhipster.validator;

import android.content.Context;
import android.content.res.Resources;
import android.widget.EditText;

import br.com.app.beardhipster.R;

/**
 * Created by cassiopaixao on 21/04/2017.
 */

public abstract class Validator {

    public static boolean formIsValid(Context context, EditText forms[]) {
        boolean valid = true;
        for (EditText et : forms) {
            if (et.getText().toString().isEmpty()) {
                et.setError(context.getString(R.string.msg_erro_obrigatorio));
                valid = false;
            }
        }
        return valid;
    }

}
